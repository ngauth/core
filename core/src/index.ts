import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { RouterModule } from '@angular/router';

import { NgCoreCoreModule } from '@ngcore/core';

// import { AuthRouteInfo } from './route/auth-route-info';
import { DefaultAuthRoutes } from './route/default-auth-routes';
import { DefaultCognitoAuthConf } from './config/default-cognito-auth-conf';

export * from './core/common-result';
export * from './core/cognito-result';
export * from './core/loggedin-status';
export * from './core/dialog-callbacks';
export * from './core/config-callbacks';
export * from './core/auth-callbacks';
export * from './common/log-item';
export * from './common/token-pair';
export * from './common/name-value-pair';
export * from './common/user-registration-info';
export * from './common/new-password-info';
export * from './route/auth-route-info';
export * from './route/default-auth-routes';
export * from './config/cognito-auth-config';
export * from './config/default-cognito-auth-conf';


@NgModule({
  imports: [
    CommonModule,
    // RouterModule,
    // // RouterModule.forRoot([{ path: '', component: SecureHomeComponent }]),
    NgCoreCoreModule.forRoot()
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class NgAuthCoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgAuthCoreModule,
      providers: [
        // AuthRouteInfo,
        DefaultAuthRoutes,
        DefaultCognitoAuthConf
      ]
    };
  }
}
