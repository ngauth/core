// TBD:
export class CommonResult {

  constructor(
    public message: (string | null) = null,
    public result: (any | null) = null,
    public extra: (any | null) = null    // extra param...
  ) {
  }
  
  public toString(): string {
    let str = '';
    // ???
    str += `message=${this.message ? this.message.toString() : ""};`;
    str += `result=${this.result ? this.result.toString() : ""};`;
    str += `extra=${this.extra ? this.extra.toString() : ""};`;
    return str;
  }

  // public toJSON(): String {
  //   return JSON.stringify(this);
  // }

}
