export class UserRegistrationInfo {
  name: string;
  email: string;
  password: string;

  constructor(
    name: (string | null) = null,
    email: (string | null) = null,
    password: (string | null) = null,
  ) {
    if (name) this.name = name;
    if (email) this.email = email;
    if (password) this.password = password;
  }

  public toString(): string {
    let str = '';
    str += `name=${this.name};`;
    str += `email=${this.email};`;
    str += `password.length=${this.password.length};`;
    return str;
  }

  public toJSON(): String {
    return JSON.stringify(this);
  }

}
