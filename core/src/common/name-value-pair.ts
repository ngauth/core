export class NameValuePair {
  name: string;
  value: string;

  constructor(
    name: (string | null) = null,
    value: (string | null) = null,
  ) {
    if (name) this.name = name;
    if (value) this.value = value;
  }

  public toString(): string {
    let str = '';
    str += `name=${this.name};`;
    str += `value=${this.value};`;
    return str;
  }

  public toJSON(): String {
    return JSON.stringify(this);
  }

}
