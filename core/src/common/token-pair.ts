export class TokenPair {
  public accessToken: string;
  public idToken: string;

  constructor(
    accessToken: (string | null) = null,
    idToken: (string | null) = null,
  ) {
    if (accessToken) this.accessToken = accessToken;
    if (idToken) this.idToken = idToken;
  }

  public toString(): string {
    let str = '';
    str += `accessToken=${this.accessToken};`;
    str += `idToken=${this.idToken};`;
    return str;
  }

  public toJSON(): String {
    return JSON.stringify(this);
  }

}
