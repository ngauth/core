export class NewPasswordInfo {
  username: string;
  oldPassword: string;
  newPassword: string;

  constructor(
    username: (string | null) = null,
    oldPassword: (string | null) = null,
    newPassword: (string | null) = null,
  ) {
    if (username) this.username = username;
    if (oldPassword) this.oldPassword = oldPassword;
    if (newPassword) this.newPassword = newPassword;
  }

  public toString(): string {
    let str = '';
    str += `username=${this.username};`;
    str += `oldPassword.length=${this.oldPassword.length};`;
    str += `newPassword.length=${this.newPassword.length};`;
    return str;
  }

  public toJSON(): String {
    return JSON.stringify(this);
  }

}
