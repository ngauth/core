import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

import { ICognitoAuthConfig, CognitoAuthConfig } from './cognito-auth-config';


// The client app needs to inherit from this class,
// and override at least loadConfig() function.

@Injectable()
export class DefaultCognitoAuthConf // implements ICognitoAuthConfig 
{
  protected authConfig: (ICognitoAuthConfig | null) = null;

  loadConfig(): Observable<ICognitoAuthConfig> {
    return Observable.create((obs) => {
      // empty implementation.
      this.authConfig = new CognitoAuthConfig();
      obs.next(this.authConfig);
    }).share();  // ???
  }

  getConfig(triggerLoading = false): (ICognitoAuthConfig | null) {
    if(triggerLoading) {
      this.loadConfig().subscribe(() => {
        // Nothing needs to be done here...
        if(dl.isLoggable()) dl.log("authConfig Loaded.");
      });
    }
    return this.authConfig;
  }

  get isLoaded(): boolean {
    return !!this.authConfig;
  }


  // These individual field getters are deprecated.
  // To be deleted

  // get region(): (string | null) {
  //   return null;
  // }

  // get identityPoolId(): (string | null) {
  //   return null;
  // }

  // get userPoolId(): (string | null) {
  //   return null;
  // }

  // get userPoolClientId(): (string | null) {
  //   return null;
  // }

  // get userIdTableName(): (string | null) {
  //   return null;
  // }

}

