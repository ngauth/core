export interface ICognitoAuthConfig {

  readonly region: (string | null);

  readonly identityPoolId: (string | null);
  readonly userPoolId: (string | null);
  readonly userPoolClientId: (string | null);

  readonly userIdTableName: (string | null);

  readonly cognitoIdpEndpoint: (string | null);
  readonly coginitoIdentityEndpoint: (string | null);
  readonly stsEndpoint: (string | null);
  readonly dynamoDbEndpoint: (string | null);

  // TBD:
  // Google, Facebook, and other id providers ???
  // ....

}

export class CognitoAuthConfig implements ICognitoAuthConfig {
  region: (string | null) = null;
  identityPoolId: (string | null) = null;
  userPoolId: (string | null) = null;
  userPoolClientId: (string | null) = null;
  userIdTableName: (string | null) = null;
  // ...
  cognitoIdpEndpoint: (string | null) = null;
  coginitoIdentityEndpoint: (string | null) = null;
  stsEndpoint: (string | null) = null;
  dynamoDbEndpoint: (string | null) = null;
  // ...
}
